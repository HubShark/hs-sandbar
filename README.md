HubShark Blog CM
================

is planned to be a fully functioning **_Blog_**ging system that can easily be 
used as a **_C_**ontent **_M_**anagemnt System.


### Features

* Twig
* Twitter Bootstrap
* jQuery
* Eu Cookie Conform
* Contact Page
* Internationalized
* English & German included
* Lanuage Switcher
* FOSUserBundle integrated
* EasyAdmin backend
* Blog with Tags
* Comments


#### How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

`git clone https://gitlab.com/HubShark/hs-sandbar.git hubshark`

**IMPORTANT!!!** Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

`cd hubshark`

Then fix the permissions like this:

```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
 
# if this doesn't work, try adding `-n` option
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
```

The next part will ask for Databank Info (you should already have them) and at the end it will ask for setting your "secret". You can get an [Online Generated key here](http://nux.net/secret).

`composer install`

answer any questions and it will or may end with errors message about an unkown databank. Fix that error with this:

`php bin/console doctrine:database:create`

<!--And then to make sure we got erverything the first time, we run the _install_ command again:

`composer install`-->


Then Create the database schema

`php bin/console doctrine:schema:create`


And now update your database:

`php bin/console doctrine:schema:update --force`


Then create your :Admin User_ (Replace _"Admin"_ with your adminuser name):

`php bin/console fos:user:create Admin --super-admin`


You need to update the fixtures

`php bin/console doctrine:fixtures:load`


And then we run this to update everything:

`composer update --with-dependencies`


_I HAVE NOT TESTED THIS YET. If you do please post an issue and let us know._


## See a screenshot...

[Visit our wiki link to see a screenshot or 2....](https://gitlab.com/HubShark/hs-sandbar/wikis/home)
